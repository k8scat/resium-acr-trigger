import sqlite3
import os
import logging

from acr.util import ding, dict_factory


def init_db(db_file, schema_file):
    conn = sqlite3.connect(db_file)
    c = conn.cursor()

    try:
        sql = 'select 1 from repo_build'
        c.execute(sql)
        return
    except Exception as e:
        logging.info('Init database')

    try:
        with open(schema_file, 'r') as f:
            sql = f.read()
        c.execute(sql)
        conn.commit()
    except Exception as e:
        ding(str(e))
        conn.rollback()
    finally:
        if conn:
            conn.close()


def add_repo_build(db_file, build):
    conn = sqlite3.connect(db_file)
    c = conn.cursor()
    try:
        sql = f'insert into repo_build(namespace, repo_name, build_id, tag, status, start_time, end_time) values(?,?,?,?,?,?,?)'
        image = build['image']
        c.execute(sql, (image['repoNamespace'],
                        image['repoName'], build['buildId'],
                        image['tag'], build['buildStatus'],
                        build.get('startTime', 0),
                        build.get('endTime', 0)))
        conn.commit()
    except Exception as e:
        ding(str(e))
        conn.rollback()
    finally:
        if conn:
            conn.close()


def get_repo_build(db_file, build_id):
    conn = sqlite3.connect(db_file)
    conn.row_factory = dict_factory
    c = conn.cursor()

    res = None
    try:
        sql = 'select id, namespace, repo_name, build_id, tag, status, start_time, end_time from repo_build where build_id=?'
        cursor = c.execute(sql, (build_id,))
        res = cursor.fetchone()
    except Exception as e:
        ding(str(e))
    finally:
        if conn:
            conn.close()
    return res
