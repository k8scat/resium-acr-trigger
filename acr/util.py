import time
import base64
import hashlib
import hmac
import json
import time
import logging
from urllib import parse

import requests
from acr import config


def ding(message, at_mobiles=None, is_at_all=False):
    """
    使用钉钉Webhook Robot监控线上系统

    :param message:
    :param at_mobiles:
    :param is_at_all:
    :return:
    """

    timestamp = round(time.time() * 1000)
    secret_enc = config.DINGTALK_SECRET.encode('utf-8')
    string_to_sign = f'{timestamp}\n{config.DINGTALK_SECRET}'
    string_to_sign_enc = string_to_sign.encode('utf-8')
    hmac_code = hmac.new(secret_enc, string_to_sign_enc,
                         digestmod=hashlib.sha256).digest()
    sign = parse.quote_plus(base64.b64encode(hmac_code))

    if at_mobiles is None:
        at_mobiles = []
    headers = {
        'Content-Type': 'application/json'
    }

    data = {
        'msgtype': 'text',
        'text': {
            'content': message
        },
        'at': {
            'atMobiles': at_mobiles,
            'isAtAll': is_at_all
        }
    }
    dingtalk_api = f'https://oapi.dingtalk.com/robot/send?access_token={config.DINGTALK_ACCESS_TOKEN}&timestamp={timestamp}&sign={sign}'
    with requests.post(dingtalk_api, data=json.dumps(data), headers=headers) as r:
        if r.status_code != requests.codes.ok:
            logging.error(f'send dingtalk message failed: {r.text}')


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
