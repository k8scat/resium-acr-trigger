import os
import json
from acr.util import ding


def get_repo_list(region, namespace, page=1, page_size=30, status=''):
    cmd = f'aliyun cr GetRepoListByNamespace --RepoNamespace {namespace} --region {region} --Page {page} --PageSize {page_size}'
    if status != '':
        cmd += f' --Status {status}'
    s = os.popen(cmd)
    res = None
    try:
        res = json.loads(s.read())
    except Exception as e:
        ding(str(e))
    return res['data']


def get_all_repo_list(region, namespace):
    page = 1
    all_repo_list = []
    while True:
        res = get_repo_list(region, namespace, page=page)
        if res is None:
            return None
        total = res['total']
        repo_list = res['repos']
        all_repo_list.extend(repo_list)
        if len(all_repo_list) >= total:
            break
        page += 1
    return all_repo_list


def get_repo_build_list(region, namespace, repo_name, page=1, page_size=30):
    cmd = f'aliyun cr GetRepoBuildList --RepoNamespace {namespace} --RepoName {repo_name} --region {region} --Page {page} --PageSize {page_size}'
    s = os.popen(cmd)
    res = None
    try:
        res = json.loads(s.read())
    except Exception as e:
        ding(str(e))
    return res['data']
