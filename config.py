REGION = 'cn-hangzhou'
NAMESPACE = 'resium'
FINISHED_BUILD_STATUSES = ['SUCCESS', 'FAILED']

DB_FILE = './data/trigger.db'
SCHEMA_FILE = './db/schema.sql'
