import sys
import logging
import json
import config

from acr.api import get_all_repo_list, get_repo_build_list
from acr.db import add_repo_build, get_repo_build, init_db
from acr.util import ding

logging.basicConfig(format="%(asctime)s %(filename)s [line:%(lineno)d] %(levelname)s - %(message)s",
                    datefmt="%Y-%m-%d %I:%M:%S",
                    level=logging.INFO)


def build_trigger(build):
    ding(json.dumps(build))


def main():
    repo_list = get_all_repo_list(config.REGION, config.NAMESPACE)
    if repo_list is None:
        sys.exit(1)

    for repo in repo_list:
        repo_name = repo['repoName']
        res = get_repo_build_list(config.REGION, config.NAMESPACE, repo_name)
        if res is None:
            sys.exit(1)

        repo_build_list = res['builds']
        if len(repo_build_list) == 0:
            continue

        repo_build = repo_build_list[0]
        repo_namespace = repo_build["image"]["repoNamespace"]
        repo_name = repo_build["image"]["repoName"]
        repo_build_tag = repo_build["image"]["tag"]
        repo_build_status = repo_build['buildStatus']
        build_id = repo_build['buildId']
        msg = f'[{repo_namespace}/{repo_name}:{repo_build_tag}] repo_build status: {repo_build_status}'
        logging.info(msg)
        logging.debug(repo_build)

        if repo_build_status in config.FINISHED_BUILD_STATUSES:
            res = get_repo_build(config.DB_FILE, build_id)
            if not res:
                add_repo_build(config.DB_FILE, repo_build)
                build_trigger(repo_build)


if __name__ == '__main__':
    init_db(config.DB_FILE, config.SCHEMA_FILE)
    main()
