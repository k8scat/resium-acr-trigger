CREATE TABLE `repo_build` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `namespace` TEXT NOT NULL,
    `repo_name` TEXT NOT NULL,
    `build_id` TEXT UNIQUE NOT NULL,
    `tag` TEXT NOT NULL,
    `status` TEXT NOT NULL,
    `start_time` INTEGER,
    `end_time` INTEGER
);
