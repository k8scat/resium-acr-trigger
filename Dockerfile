FROM python:3.7-alpine
LABEL maintainer="hsowan <hsowan.me@gmail.com>"

# Install aliyun-cli
ENV ALIYUN_CLI_VERSION=3.0.73
RUN apk add --no-cache curl && \
    curl -LO https://github.com/aliyun/aliyun-cli/releases/download/v${ALIYUN_CLI_VERSION}/aliyun-cli-linux-${ALIYUN_CLI_VERSION}-amd64.tgz && \
    tar -C /usr/local/bin -zxf aliyun-cli-linux-${ALIYUN_CLI_VERSION}-amd64.tgz && \
    chmod +x /usr/local/bin/aliyun && \
    rm -f aliyun-cli-linux-${ALIYUN_CLI_VERSION}-amd64.tgz && \
    apk del curl && \
    mkdir /lib64 && \
    ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2

WORKDIR /data/resium-acr-trigger
COPY . .
RUN mkdir data && \
    pip install -r requirements.txt && \
    echo "* * * * * cd /data/resium-acr-trigger && python main.py" > /etc/crontabs/root

CMD ["crond", "-f", "-l", "0"]
